package com.burak.storical.ui.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.burak.storical.R;
import com.burak.storical.databinding.ActivityMainBinding;
import com.burak.storical.ui.fragment.BaseFragment;
import com.burak.storical.ui.fragment.CreateFragment;
import com.burak.storical.ui.fragment.MoreFragment;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        final BaseFragment createFragment = new CreateFragment();
        final BaseFragment moreFragment = new MoreFragment();

        openFragment(createFragment);

        binding.tablayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (binding.tablayout.getSelectedTabPosition() == 0) {
                    // Create Fragment
                    openFragment(createFragment);

                } else {
                    // More Fragment
                    openFragment(moreFragment);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void openFragment(BaseFragment baseFragment){
        getSupportFragmentManager().beginTransaction().replace(binding.fragmentContainer.getId(), baseFragment).commit();
    }
}
