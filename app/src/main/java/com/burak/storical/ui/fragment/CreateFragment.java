package com.burak.storical.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.burak.storical.R;
import com.burak.storical.databinding.FragmentCreateBinding;
import com.burak.storical.ui.fragment.selection.BackgroundSelectionFragment;
import com.burak.storical.ui.fragment.selection.IconSelectionFragment;
import com.burak.storical.ui.fragment.selection.SettingsSelectionFragment;

public class CreateFragment extends BaseFragment {

    FragmentCreateBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create, container, false);

        final BaseFragment backgroundFragment = new BackgroundSelectionFragment();
        final BaseFragment iconFragment = new IconSelectionFragment();
        final BaseFragment settingsFragment = new SettingsSelectionFragment();

        openSelection(backgroundFragment);

        binding.btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Background
                openSelection(backgroundFragment);
            }
        });

        binding.btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Icon
                openSelection(iconFragment);
            }
        });

        binding.btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Settings
                openSelection(settingsFragment);
            }
        });

        binding.btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Save
            }
        });

        return binding.getRoot();
    }

    private void openSelection(BaseFragment baseFragment) {
        getChildFragmentManager().beginTransaction().replace(binding.containerSelectionFragment.getId(), baseFragment).commit();

    }
}
