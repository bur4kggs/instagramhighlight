package com.burak.storical.ui.fragment.selection;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.burak.storical.R;
import com.burak.storical.databinding.FragmentIconBinding;
import com.burak.storical.ui.fragment.BaseFragment;

public class IconSelectionFragment extends BaseFragment {

    FragmentIconBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_icon, container, false);

        return binding.getRoot();
    }

}
